package com.poc.jpa.joins.entities;

import javax.persistence.*;

@Entity
@Table
public class Student {
    @Id
    private Long id;

    private String name;

    @OneToOne
    @JoinColumn(name = "addr_id")
    private Address address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
