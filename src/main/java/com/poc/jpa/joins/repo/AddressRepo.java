package com.poc.jpa.joins.repo;

import com.poc.jpa.joins.entities.Address;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepo extends CrudRepository<Address, Long> {
}
