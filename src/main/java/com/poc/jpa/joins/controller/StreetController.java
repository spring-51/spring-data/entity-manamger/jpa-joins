package com.poc.jpa.joins.controller;

import com.poc.jpa.joins.entities.Street;
import com.poc.jpa.joins.entityManager.StreetEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/streets")
public class StreetController {

    @Autowired
    private StreetEntityManager streetEntityManager;

    @GetMapping("{id}")
    public String getById(@PathVariable Long id){
        return streetEntityManager.getById(id);
    }

    @GetMapping("/students/{studentId}")
    public String getByStudentId(@PathVariable Long studentId){
        return streetEntityManager.getByStudentId(studentId);
    }
}
