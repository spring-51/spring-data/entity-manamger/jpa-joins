package com.poc.jpa.joins.bootstrap;

import com.poc.jpa.joins.entities.Address;
import com.poc.jpa.joins.entities.Street;
import com.poc.jpa.joins.entities.Student;
import com.poc.jpa.joins.repo.AddressRepo;
import com.poc.jpa.joins.repo.StreetRepo;
import com.poc.jpa.joins.repo.StudentRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class DataLoder implements CommandLineRunner {

    private final StudentRepo studentRepo;
    private final AddressRepo addressRepo;
    private final StreetRepo streetRepo;

    public DataLoder(StudentRepo studentRepo, AddressRepo addressRepo, StreetRepo streetRepo) {
        this.studentRepo = studentRepo;
        this.addressRepo = addressRepo;
        this.streetRepo = streetRepo;
    }

    @Transactional
    @Override
    public void run(String... args) throws Exception {
        Street street = new Street();
        street.setId(3l);
        street.setName("Pink city road");
        street = streetRepo.save(street);

        Address address = new Address();
        address.setId(2l);
        address.setStreet(street);
        address = addressRepo.save(address);

        Student student = new Student();
        student.setId(1l);
        student.setName("Wakad");
        student.setAddress(address);
        studentRepo.save(student);
    }
}
