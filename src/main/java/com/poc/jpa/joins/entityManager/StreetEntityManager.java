package com.poc.jpa.joins.entityManager;

import com.poc.jpa.joins.entities.Street;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class StreetEntityManager {
    @Autowired
    private EntityManager entityManager;

    public String getById(Long id){
        String queryStr = "SELECT str.name, str.id FROM Student stu INNER JOIN stu.address a INNER JOIN a.street str  WHERE str.id =  ?1";
        TypedQuery<Object[]> query = entityManager.createQuery(queryStr, Object[].class);
        query.setParameter(1,id);
        List<Object[]> resultList = query.getResultList();
        return (String)resultList.get(0)[0];
    }

    public String getByStudentId(Long studentId) {
        String queryStr =
                "SELECT str.name, str.id FROM Student stu " +
                "INNER JOIN stu.address a " +
                "INNER JOIN a.street str " +
                "WHERE stu.id =  ?1";
        TypedQuery<Object[]> query = entityManager.createQuery(queryStr, Object[].class);
        query.setParameter(1,studentId);
        List<Object[]> resultList = query.getResultList();
        return (String)resultList.get(0)[0];
    }
}
