package com.poc.jpa.joins.entities;

import javax.persistence.*;

@Entity
@Table
public class Address {
    @Id
    private Long id;

    @OneToOne
    @JoinColumn(name = "street_id")
    private Street street;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }
}
