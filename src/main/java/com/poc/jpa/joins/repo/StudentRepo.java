package com.poc.jpa.joins.repo;

import com.poc.jpa.joins.entities.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepo extends CrudRepository<Student, Long> {
}
