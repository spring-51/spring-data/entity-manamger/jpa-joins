package com.poc.jpa.joins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaJoinsApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaJoinsApplication.class, args);
	}

}
