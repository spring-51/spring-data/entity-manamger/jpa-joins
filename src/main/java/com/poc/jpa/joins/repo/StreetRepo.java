package com.poc.jpa.joins.repo;

import com.poc.jpa.joins.entities.Street;
import org.springframework.data.repository.CrudRepository;

public interface StreetRepo extends CrudRepository<Street,Long> {
}
